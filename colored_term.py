"""
Ansii escape codes are magic strings that the terminal interprets as commands
Some of the most ab-used ones change the color of the text
They begin with the character 27 = 0x1b.
Python strings can contain hex escapes so chr(27) == '\x1b'
The syntax of ansii escapes is
\x1b[21m
\x1b[34m
etc. In general \x1b[numberm
"""

def toesc(c):
    """
    Returns the ansii escape with the code c
    \x1b[cm /// LBLACK=toesc(90)...
    """
    return '\x1b[' + str(c)+'m'

# codes from 90 to 98 and from 30 to 38 change the text's color
# code zero 0 resets the terminal to the default state

# define the reset code
ENDC = toesc(0)
# define the following colors corresponding to the code range 90 98
#LBLACK, LRED, LGREEN, LYELLOW, LBLUE, LPURPLE, LCYAN, LWHITE
# define the following colors corresponding to the code range 30 38
#DBLACK, DRED, DGREEN, DYELLOW, DBLUE, DPURPLE, DCYAN, DWHITE

LBLACK = toesc(90)
LRED = toesc(91)
LGREEN = toesc(92)
LYELLOW = toesc(93)
LBLUE = toesc(94)
LPURPLE = toesc(95)
LCYAN = toesc(96)
LWHITE = toesc(97)
DBLACK = toesc(30)
DRED = toesc(31)
DGREEN = toesc(32)
DYELLOW = toesc(33)
DBLUE = toesc(34)
DPURPLE = toesc(35)
DCYAN = toesc(36)
DWHITE = toesc(37)

def colored_text(text, color):
    """ returns the text surrounded by ansii escapes that would make it print in the given color on a terminal """

    return color+text+ENDC

def welcome_python():
    """ prints Python welcomes the terminal. Each work in a different color"""
    import random
    s='Python welcomes the terminal'
    sir=s.split((' '))
    culori=[LBLACK, LRED, LGREEN, LYELLOW, LBLUE, LPURPLE, LCYAN, LWHITE,DBLACK, DRED, DGREEN, DYELLOW, DBLUE, DPURPLE, DCYAN, DWHITE]
    for st in sir:
        print colored_text(st,random.choice(culori))

welcome_python()


