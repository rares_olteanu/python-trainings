def un_flatten(paths):
    d = {}
    for p in paths:
        lvl=d
        print lvl
        print d
        for itm in p.split('/'):
            print 'itm={}'.format(itm)
            if itm not in lvl:
                lvl[itm] = {}
            print 'lvl[itm]={}'.format(lvl[itm])
            lvl = lvl[itm]
            print 'lvl={}'.format(lvl)
    return d


def test_un_flatten():
    tree = un_flatten(['A/B/T', 'A/U', 'A/U/Z'])
    assert tree == {
        'A': {
            'B': {'T': {}},
            'U': {
                'Z': {}
            }
        }
    }
    assert tree['A']['B']['T'] == {}

print un_flatten(['A/B/T', 'A/U', 'A/U/Z'])