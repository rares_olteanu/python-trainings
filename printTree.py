
import os
import sys

def printTree(cale, ident=0):
    for name in os.listdir(cale):
        path = os.path.join(cale, name)
        print '|  '*(ident) + '| -'+name
        if(os.path.isdir(path)):
            printTree(path, ident+1)

def main(argv):
    if len(argv)>1:
        os.chdir(argv[1])
        cale = argv[1]
    else:
        cale = os.getcwd()
    try:
        print cale
        printTree(cale)
    except Exception:
        print "Error"

if __name__=="__main__":
    main(sys.argv)



