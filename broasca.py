import turtle

t=turtle.Turtle()

def drawSquare(x,y,l):
    t.setheading(0)
    t.penup()
    t.goto(x, y)
    t.pendown()
    for i in xrange(4):
        t.forward(l)
        t.right(90)


def draw(x,y,l):
    if l<50:
        return
    drawSquare(x+border, y-border, l/2-2*border)
    l = l / 2 - 4* border
    draw(x+2*border,y-2*border,l)
    draw(x + l / 2 + 2*border, y - 2*border, l)
    draw(x + l / 2 + 2*border, y - l / 2 - 2*border, l)

def f(x,y,l):
    draw(x, y, l)
    draw (x+l/2+border,y,l)
    draw (x+l/2+border, y-l/2,l)



turtle.setup(600,600)
screen=t.getscreen()

maxy= screen.window_height()
maxx= screen.window_width()
border=10
l=maxx-2*border
myx=-maxx/2+border
myy= maxy/2-border
t.speed(10)
f(myx, myy, l)
turtle.done()