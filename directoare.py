import os
import sys

def colored_text(text, color):

    return '\x1b[' + str(color) + 'm'+text+'\x1b[' + str(0) + 'm'

def getDirectories(cale, tip):
    l=[]
    for name in os.listdir(cale):
        path = os.path.join(cale, name)
        if tip=='directoare':
            if os.path.isdir(path):
                l.append(name)
        elif tip=='fisiere':
            if os.path.isfile(path):
                l.append(name)
    return sorted(l)

def numarfisiere(cale):
    return len([f for f in os.listdir(cale) if os.path.isfile(os.path.join(cale, f))])

def getsize(size):
    terminatie={1:'KB',2:'MB',3:'GB'}
    if size<1024:
        return '{:7} {}'.format(str(size), 'B')
    i=1
    while size>1024 and i<3:
        size=float(size)/1024
        i+=1
    return '{:6.3f} {:>3}'.format(size,terminatie[i-1])

def scrie(cale):
    directoare=getDirectories(cale, 'directoare')
    fisiere=getDirectories(cale, 'fisiere')
    print "current dir: "+cale
    if len(directoare)>1:
        for x in directoare:
            print colored_text('{:20} {:>3} {}'.format(x+'/   ',str(numarfisiere(x)), ' items'), 94)
    if len(fisiere) > 1:
        for x in fisiere:
            if(not x.startswith('.')):
                #not hidden file
                print colored_text('{:15} {:>5} {:>10} '.format(x.split('.')[0],'.'+ x.split(".")[-1],\
                                                                getsize(os.path.getsize(x))), 32)
            else:
                #hidden file
                print colored_text('{:15} {:>5} {:>10} '.format(x.split('.')[0], '.' + x.split(".")[-1],\
                                                                getsize(os.path.getsize(x))),30)

def main(argv):
    if len(argv)>1:
        os.chdir(argv[1])
        cale = argv[1]
    else:
        cale = os.getcwd()
    try:
        scrie(cale)
    except Exception:
        print "Error"

if __name__=="__main__":
    main(sys.argv)


