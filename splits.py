import string

def splits(word):
    return [(word[:i],word[len(word[:i]):]) for i in range(len(word)+1)]


def deletes(word, checkWord):
    return len([x for x in splits(word) if x[0] == checkWord])>0


def transpune(s):
    return [x + y[1] + y[0] +y[2:] for x, y in splits(s) if len(y) > 1]

def substitue(c,s):
    return [x+ch+y for x,y in splits(s) for ch in c]

def inserare(c,s):
    return[x[0]+y+x[1] for x in splits(s) for y in c]

c=list(string.ascii_lowercase)
print splits('anax')
print inserare('anax')
print substitue(c,'anax')
print transpune('anax')



#assert splits("ana")==[('', 'ana'), ('a', 'na'), ('an', 'a'), ('ana', '')]
#assert deletes('rocket', 'rockets') == False
#assert deletes('rockets', 'rocket') == False
#assert deletes('rocket', 'rocket') == True
#assert deletes('', '') == True