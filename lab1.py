# Exercises for notions learned in unit 1
# ---------------------------------------

# takes a message, capitalizes the first letter then underlines the message with -
# print make_title('ana')
# Ana
# ---
# use the string method upper
def make_title(message):
    x=message[0].upper()+message[1:]+ '\n'+ "-"*3
    return str(x)

# given a list of tuples of length 2 it returns a list of the tuples inverted
# if both values in the tuple are the same then we omit them from the output
# [(2, 4), (3, 3), (1, 2)] will become [(4, 2), (2, 1)]
def invert_tuples(lst):
    a=[]
    for x, y in lst:
        if x!=y:
            x, y = y, x
            a.append((x,y))
    return a

# curses is a dictionary of cursewords to censored curses
# Given a list of words replace the curses according to the dict
# censor(['you', 'fucker'], {'fucker': 'f**er'}) should return
# ['you', 'f**er']
def censor(lst, curses):
    for i,x in enumerate(lst):
        if x in curses:
            lst[i]=curses[x]
    return lst



# Given a string s, return a string made of the first 2
# and the last 2 chars of the original string,
# so 'spring' yields 'spng'. However, if the string length
# is less than 2, return instead the empty string.
def both_ends(s):
    sir=''
    if len(s)<2:
        return ''
    else:
        return s[0:2]+s[len(s)-2:len(s)]

# return the factorial of n
# factorial(5) = 1*2*3*4*5
def factorial(n):
    a=[]
    if n in a:
        return n
    elif n==0:
         return 1
    else:
        a.append(n)
    return n*factorial(n-1)

# return a list of fibonacci numbers smaller than n
def fibo(n):
    x = [0, 1]
    i=2
    while(x[i-1]+x[i-2]<=n):
        x.append(x[i-1]+x[i-2])
        i+=1
    return x


#----- testing code below ---

def test_make_title():
    assert make_title('ana') == 'Ana\n---'

def test_invert_tuples():
    assert invert_tuples([(2, 4), (3, 3), (1, 2)]) == [(4, 2), (2, 1)]

def test_censor():
    assert censor(['you', 'fucker'], {'fucker': 'f**er'}) == ['you', 'f**er']

def test_both_ends():
    assert both_ends('spring') == 'spng'
    assert both_ends('Hello') == 'Helo'
    assert both_ends('a') == ''
    assert both_ends('xyz') == 'xyyz'

def test_fibo():
    assert fibo(10) == [0, 1, 1, 2, 3, 5, 8]

def test_factorial():
    assert factorial(5) == 1*2*3*4*5
    assert factorial(1) == 1
    assert factorial(0) == 1

