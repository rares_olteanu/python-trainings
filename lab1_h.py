# Homework problems for unit 1
# ----------------------------

#The rest of dividing a square number like 81, by 32 can only be
#0, 1, 4, 9, 16, 17, 25
#A number that has such a rest when divided by 32 is called a pseudo square mod 32
def pseudo_square_mod32(n):
    resturi=[0,1,4,9,16,17,25] #pt liste mari mai bine set{0,1,4,8,17,25}
    return n%32 in resturi

def certain_square(n):
    return int(n**0.5 + 0.5)**2 == n

# Test how useful is the maybe_square test.
# How many false positives it reports for numbers under 1000?
# What percentage of tests are false positives?
def maybe_square_accuracy():
    N = 10000
    nr = 0
    for i in xrange(N):
        if pseudo_square_mod32(i) and not certain_square(i):
            nr += 1
    print '{} false positive under {} percent {}%'.format(nr, N, 100*nr/N)
    return nr


# Given a list of numbers, return a list where
# all adjacent == elements have been reduced to a single element,
# so [1, 2, 2, 3] returns [1, 2, 3]. You may create a new list or
# modify the passed in list.
def remove_adjacent(nums):
    rez=[]
    if len(nums)==0:
        return []
    elif len(nums)==1:
        return nums[0]
    else:
        rez.append(nums[0])
        for i in range(1,len(nums)):
            if nums[i]!=nums[i-1]:
                rez.append(nums[i])
    return rez

# Receives a color intensity as a int.
# 0 is black, 255 is the most saturated red, 128 is a middle red
# It should return the color in a float space where 0.0 is black
# and 1.0 is the most saturated red. 0.5 is that middle red.
# int_color_to_float_color(128) == 0.5
def int_color_to_float_color(red):
    return float(red)/255
# similar to the above case, but now we want to map black to tm
# and the reddest of reds, the 255 to tM
# int_color_to_float_range(128, 0, 2) == 1.0

def int_color_to_float_range(red, tm, tM):
    # [min, max]->[a,b]-->f(x)=(((b-a)*(x-min))/(max-min))+a
    return (float(red*(tM-tm))/255) + tm


# rescale transforms a number from the range fm..fM to the tm..tM range
# This is the generalization of the color use case
# but now f ranges not from 0..255 but from fm..fM
def rescale(f, fm, fM, tm, tM):
    f = float(f)
    return (float((f-fm) * (tM - tm)) / (fM-fm)) + tm
    return f

def test_int_color_to_float_color():
    assert abs(int_color_to_float_color(51) - 0.2) < 1e-2
    assert abs(int_color_to_float_color(155) - 0.6078) < 1e-2

def test_int_color_to_float_range():
    assert abs(int_color_to_float_range(51, 1, 2) - 1.2) < 1e-2
    assert abs(int_color_to_float_range(128, -2, 2) - 0) < 1e-2

def test_pseudo_square_mod32():
    assert pseudo_square_mod32(9)
    assert not pseudo_square_mod32(24)
    assert pseudo_square_mod32(17)

def test_remove_adjacent():
    assert remove_adjacent([1, 2, 2, 3]) == [1, 2, 3]
    assert remove_adjacent([2, 2, 3, 3, 3]) == [2, 3]
    assert remove_adjacent([]) == []

def test_rescale():
    assert rescale(5, 0, 10, 0, 1) == 0.5
    assert abs(rescale(3, 2, 7, -1, 2) + 0.4) < 1e-4

def test_maybe_square_accuracy():
    assert maybe_square_accuracy()!=0