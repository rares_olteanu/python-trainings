from DB import DB
import datetime
from getPassedTime import timePassed
import sys





def showHelp():
    print 'positional arguments: {} {} {}'.format('list','add','done')

allowedCommands=['done','list','add']

def main():
    x = DB('./db/FirstDb.db')
    if len(sys.argv)<2 or len(sys.argv)>4 or sys.argv[1] not in allowedCommands:
        showHelp()
        sys.exit(1)
    else:
        if sys.argv[1]=='done':
            if str(sys.argv[2]).isdigit():
                if x._ensure_id(int(sys.argv[2])):
                    x.changeStatus(int(sys.argv[2]))
                    d=x.get(int(sys.argv[2]))
                    print 'status changed for the task with the id={}'.format(str(sys.argv[2]))
                    print d
                else:
                    print 'id: {} not found'.format(str(sys.argv[2]))
            else:
                print 'Second parameter shold be number'
        if sys.argv[1]=='list':
            v = [y for y in x.load() if str(y['done']) == 'False']
            for e in v:
                print 'id {} {}'.format(e['id_'], timePassed(e['created']))
                print '   {}'.format(e['message'])
        if sys.argv[1]=='add':
            mes=str(sys.argv[2])
            myId=x.getID()
            dat=datetime.datetime.now().strftime("%y-%m-%d-%H-%M")
            y=(myId,mes,'False',dat)
            x.add(y)
            print 'task id={} message={} created in {}'.format(myId,mes,dat)




main()







