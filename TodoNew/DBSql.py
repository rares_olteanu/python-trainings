import sqlite3

class DBSql(object):

    def __init__(self, db):
        self.db = db

    def create_connection(self):
        try:
            conn = sqlite3.connect(self.db)
            return conn
        except Exception  as e:
            print(e.message)
        return None

    def load(self):
        '''loads all data file'''

        data = []
        x=DBSql(self.db)
        conn = x.create_connection()
        if conn is not None:
            cursor = conn.cursor()
            cursor.execute("SELECT * FROM tasks")
            rows = cursor.fetchall()
            for row in rows:
                data.append({'id_': row[0],
                            'message': row[1],
                            'done': row[2],
                            'created': row[3]
                            })
        conn.close()
        return data

    def _ensure_id(self,t):
        '''check if id exists'''

        x = DBSql(self.db)
        conn = x.create_connection()
        if conn is not None:
            cursor = conn.cursor()
            cursor.execute("SELECT * FROM tasks WHERE id=?", (t,))
            rows = cursor.fetchall()
        conn.close()
        return len(rows)>0

    def getID(self,t=1):
        '''return a valid id'''

        x = DBSql(self.db)
        l=x.load()
        v = [y['id_'] for y in l]
        if t in v:
            res=max(v)+1
        else:
            res = t
        return res


    def changeStatus(self, t):
        '''sets done=true to a task'''

        x = DBSql(self.db)
        l = x.load()
        myId= [y['id_'] for y in l if y['id_'] == t]
        conn = x.create_connection()
        cursor = conn.cursor()
        if conn is not None:
            st="True"
            cursor.execute("UPDATE tasks SET done=? WHERE id =?", (st, myId[0]))
            conn.commit()
        conn.close
        return cursor.lastrowid

    def add(self, task):
        '''adds new task'''

        x = DBSql(self.db)
        conn = x.create_connection()
        if conn is not None:
            cursor = conn.cursor()
        comm = '''INSERT INTO tasks(id,message,done,created)
                  VALUES(?,?,?,?)'''
        cursor.execute(comm,task)
        conn.commit()
        conn.close
        return cursor.lastrowid

    def get(self, id):
        '''gets the task with the specific id'''

        x = DBSql(self.db)
        conn = x.create_connection()
        if conn is not None:
            cursor = conn.cursor()
            cursor.execute("SELECT * FROM tasks WHERE id=?", (id,))
            row = cursor.fetchall()
        conn.close()
        if len(row)>0:
            return row
        else:
            raise Exception('Id '+str(id)+ ' not found')




