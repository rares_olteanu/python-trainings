
import datetime


def timePassed(dtS):
    res=''
    dtN = datetime.datetime.now().strftime("%y-%m-%d-%H-%M")
    dateNow = datetime.datetime.now().strptime(dtN, "%y-%m-%d-%H-%M").date()
    dateStart = datetime.datetime.strptime(dtS, "%y-%m-%d-%H-%M").date()
    timeNow = datetime.datetime.strptime(dtN, "%y-%m-%d-%H-%M").time()
    timeStart = datetime.datetime.strptime(dtS, "%y-%m-%d-%H-%M").time()

    if dateNow.year-dateStart.year==0:
        if dateNow.month-dateStart.month==0:
            if dateNow.day-dateStart.day==0:
                if(timeNow.hour-timeStart.hour)==0:
                    res+='todo {} minutes ago'.format(timeNow.minute-timeStart.minute)
                else:
                    res+='todo {} hours ago'.format(timeNow.hour-timeStart.hour)
            else:
                res+='todo {} days ago'.format(dateNow.day-dateStart.day)
        else:
            res += 'todo {} months ago'.format(dateNow.month - dateStart.month)
    else:
        res += 'todo {} years ago'.format(dateNow.year - dateStart.year)

    return res