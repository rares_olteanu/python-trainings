import json
import os
import datetime
from Todo import Todo

class DBJson(object):
    datefmt = ''
    def __init__(self, db):
        self.db = db

    def load(self):
        '''loads all data file'''
        if not os.path.exists(self.db):
            raise Exception('{:s} does not exist.'.format(self.db))
        else:
            if os.stat(self.db).st_size == 0:
                return None
            else:
                with open(str(self.db), 'r') as fil:
                    data = json.load(fil)
        return data

    def _ensure_id(self,t):
        '''check if id exists'''
        x=DBJson('data.txt')
        l=x.load()
        if l!=None:
            return t in [y['id_'] for y in l]
        else:
            return True

    def getID(self,t=1):
        '''return a valid id'''
        x = DBJson(self.db)
        l = x.load()
        if l != None:
            v=[y['id_'] for y in l]
            if t in v:
                res=max(v)+1
            else:
                res = t
        else:
            res = t
        return res


    def _todo_to_json(self, t):
        '''sets done=true to a task'''
        x = DBJson(self.db)
        l=x.load()
        v = [y for y in l if y['id_'] == t]
        v[0]['done']=True
        with open(self.db, 'w') as fil:
            fil.write(json.dumps(l, indent=2))



    def add(self, t):
        '''adds new task'''
        x=DBJson(self.db)
        dt=x.load()
        myId=x.getID(t.id_)
        data={
            'id_': myId,
            'message': t.message,
            'done': t.done,
            'created': datetime.datetime.now().strftime("%y-%m-%d-%H-%M")
        }
        if dt == None:
            dt = [data]
        else:
            dt.append(data)
        with open(self.db, 'w') as fil:
            fil.write(json.dumps(dt, indent=2))


    def get(self, id):
        '''gets the task with the specific id'''
        x=DBJson(self.db)
        l=x.load()
        res={}
        if l!=None:
            if x._ensure_id(id):
                v = [y for y in l if y['id_'] == id]
                res={
                    'id_': v[0]['id_'],
                    'message': v[0]['message'],
                    'done': v[0]['done'],
                    'created': v[0]['created']
                }
            else:
                raise Exception('Id '+str(id)+ ' not found')
        else:
            raise Exception('empty data file '+str(x.db))
        return res




