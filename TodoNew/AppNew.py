from DBJson import DBJson
from DBSql import DBSql
from getSettings import getSettings
from getPassedTime import timePassed
from Todo import Todo
import datetime
from writexml import writexml

import argparse
import sys

def listTasks(args, storage,mypath):

    l=[]
    if storage == 'json':
        db=DBJson(mypath)
    if storage=='sqlite':
        db=DBSql(mypath)
    l=db.load()
    v = [y for y in l if str(y['done']) == 'False']
    if args.format is None:
        for e in v:
            print 'id {} {}'.format(e['id_'], timePassed(e['created']))
            print '   {}'.format(e['message'])
    else:
        if args.format.split('=')[-1]=='xml':
            writexml(l,'xmldata.xml')
        if args.format.split('=')[-1]=='html':
            writexml(l,'htmldata.html')




def addTask(message,storage,mypath):
    mes = message.message
    dat = datetime.datetime.now().strftime("%y-%m-%d-%H-%M")
    if storage == 'json':
        db=DBJson(mypath)
        myId = db.getID()
        y = Todo(mes, myId)
        db.add(y)
        print 'tast id={} message={} created in {}'.format(myId, mes, dat)

    if storage=='sqlite':
        db=DBSql(mypath)
        mes = str(sys.argv[2])
        myId = db.getID()
        y = (myId, mes, 'False', dat)
        db.add(y)
        print 'task id={} message={} created in {}'.format(myId, mes, dat)

def doneTask(x,storage,mypath):
    id_=x.id
    if storage == 'json':
        db=DBJson(mypath)
        if db._ensure_id(id_):
            db._todo_to_json(id_)
            d = db.get(id_)
            print 'status changed for the task with the id={}'.format(id_)
            print d
    if storage=='sqlite':
        db=DBSql(mypath)
        if db._ensure_id(id_):
            db.changeStatus(id_)
            d = db.get(id_)
            print 'status changed for the task with the id={}'.format(id_)
            print d



def main():
    storage = getSettings()['STORAGE_TYPE']
    mypath = getSettings()['STORAGE_PATH']

    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(title='actions')

    parser_list = subparsers.add_parser('list', help='list not finished tasks')
    parser_list.add_argument('-format','--format', help="Specify id of the task", type=str)
    parser_list.set_defaults(func=listTasks)

    parser_add = subparsers.add_parser('add', help='adds a task')
    parser_add.add_argument('message', help="Specify id of the task", type=str)
    parser_add.set_defaults(func=addTask)

    parser_done = subparsers.add_parser('done', help='complete a task')
    parser_done.add_argument('id', help="Specify id of the task", type=int)
    parser_done.set_defaults(func=doneTask)

    if len(sys.argv) <= 1:
        sys.argv.append('--help')
    options=parser.parse_args()
    options.func(options,storage,mypath)


main()