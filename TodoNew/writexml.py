
import xml.etree.cElementTree as elem

def writexml(l,fil):
    root = elem.Element("tasks")
    for x in l:
        task = elem.SubElement(root, "task")
        elem.SubElement(task, "id", name='id').text=str(x['id_'])
        elem.SubElement(task, "message", name='message').text = x['message']
        elem.SubElement(task, "done", name='done').text = x['done']
        elem.SubElement(task, "created", name='created').text = x['created']
        tree = elem.ElementTree(root)
    tree.write(open(fil, 'w'))