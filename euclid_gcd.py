def gcd(a, b):
    while b != 0:
        t = b
        b = a % b
        a = t
    return a


def gcd1(a, b):
    while b!=0:
        a, b = b, a % b
    return a


assert gcd1(270,192)==6
assert gcd1(1071,462)==21
assert gcd1(100,17)==1
