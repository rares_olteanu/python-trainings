
import re

def cleanit(word):
    if word.startswith("'") or word.endswith("'"):
        word.replace("'", '')
    word = re.sub(r'[?|$)":;.\'\-!^,(`]+', '', word)
    return word.lower()

def wordcounts():
    c={}
    f=open('corpus.txt')
    for word in f.read().split():
        word=cleanit(word)
        if word not in c:
            c[word] = 1
        else:
            c[word] += 1
    return c

def main():
    d=wordcounts()
    d1= [(v, k) for k, v in d.iteritems()]
    d1.sort(reverse=True)
    for v, k in d1:
        print "%s: %d" % (k, v)



main()