
import argparse

def sum(n):
    return n+5



def main():
    parser=argparse.ArgumentParser()
    parser.add_argument()




    if len(sys.argv)<2 or sys.argv[1] not in ['list-artists', 'long-tracks']:
        showHelp()
        sys.exit(1)
    else:
        if sys.argv[1]=='list-artists':
            if len(sys.argv)>2:
                if sys.argv[2] == '--page':
                    if sys.argv[3].isdigit():
                        a = [x['Name'] for x in getArtistsPage(int(sys.argv[3]))]
                        printArtists(a)
                    else:
                        print 'must imput the page number'
                else:
                    print 'second parameter should be --page'
            else:
                print 'must enter --page number'

        if sys.argv[1]=='long-tracks':
            printTracks(getArtistsTracks(7))
            if len(sys.argv)>2 and sys.argv[2] == '--output':
                if len(sys.argv)>3:
                    fis=str(sys.argv[3])
                    if fis.split('.')[-1]!='csv':
                        fis+='.csv'
                    scrieCsv(fis,getArtistsTracks(7))
