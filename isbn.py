# this function should return a tuple
# the first element is a list of integers representing the isbn
# the second element is the check digit
# parse_isbn('ISBN817525766-0') == ([8, 1, 7, 5, 2, 5, 7, 6, 6], 0)

def parse_isbn(isbn):
    isbn_check = int(isbn[-1])
    isbn_main = [int(x) for x in isbn[4:4+9]]
    return isbn_main, isbn_check

def is_isbn_valid(isbn):
    s=0
    j=10
    i=0
    l=[int(x) for x in isbn[4:4+9]]
    while i<len(isbn[4:4+9]):
        s+=j*l[i]
        j-=1
        i+=1
    return s % 11 == 0
# cu zip - sum(x*i for x,i in zip(isbn,j)) - j=range[10:2:-1]
# py.test code below

def test_parse_isbn():
    assert parse_isbn('ISBN817525766-0') == ([8, 1, 7, 5, 2, 5, 7, 6, 6], 0)

def test_is_isbn_valid():
    assert is_isbn_valid('ISBN817525766-0')
    assert not is_isbn_valid('ISBN817525767-0')

#print is_isbn_valid('ISBN817525766-0')