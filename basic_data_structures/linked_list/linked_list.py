from node import Node


class LinkedList(object):

    def __init__(self):
        self.first = None

    def insert_start(self, data):

        new_node = Node(data)

        if not self.first:
            self.first = new_node
        else:
            new_node.next_node = self.first
            self.first = new_node

    def insert_end(self, data):
        if not self.first:
            self.insert_start(data)
            return
        else:
            new_node = Node(data)
            current_node = self.first
            while current_node.next_node is not None:
                current_node = current_node.next_node
            current_node.next_node = new_node

    def insert_after(self, item, data):
        if not self.first:
            self.insert_start(data)
            return
        else:
            new_node = Node(data)
            current_node = self.first
            while current_node.data != item and current_node.next_node is not None:
                current_node = current_node.next_node
            new_node.next_node = current_node.next_node
            current_node.next_node = new_node

    def print_list(self):
        item = self.first
        while item is not None:
            print('{}'.format(item.data), end=' ')
            item = item.next_node

    def remove(self, data):
        if self.first:
            current = self.first
            the_next_one = current.next_node
            if current.data == data:
                self.first = the_next_one
                current.delete_node()
            else:
                while the_next_one.next_node and the_next_one.data != data:
                    current = current.next_node
                    the_next_one = the_next_one.next_node
                if not the_next_one.next_node and the_next_one.data == data:
                    current.next_node = None
                    the_next_one.delete_node()
                elif the_next_one.next_node and the_next_one.data == data:
                    current.next_node = the_next_one.next_node
                    the_next_one.delete_node()
                else:
                    raise TypeError('\n{} not in the list'.format(data))



