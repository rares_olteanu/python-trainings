

class Node(object):
    def __init__(self, data):
        self.data = data
        self.next_node = None

    def delete_node(self):
        del self.data
        del self.next_node

