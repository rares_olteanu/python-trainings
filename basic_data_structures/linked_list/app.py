from linked_list import LinkedList


def main():

    my_list = LinkedList()

    my_list.insert_start(22)

    my_list.insert_start(13)

    my_list.insert_end(15)

    my_list.insert_after(50, 14)

    my_list.print_list()

    try:
        my_list.remove(66)
    except TypeError as e:
        print(e)

    print("\nafter remove")
    my_list.print_list()


if __name__ == '__main__':
    main()
