
import argparse
from chinookSqlRepo import getArtistsPage, getArtistsTracks

def showHelp():
    print 'positional arguments: {} {} {}'.format('list-artists','long-tracks')

def printCell(x,lung):
    linie = '+ ' + '-' * lung + ' +'
    print linie
    print '| ' + x + ' ' * (lung - len(x)) + ' |'

def printArtists(l):
    lung = max(len(x) for x in l)
    for x in l:
        printCell(x,lung)
        print '+ ' + '-' * lung + ' +'

def printTracks(l):

    lArt = max(len(x['artist']) for x in l)
    lTrc = max(len(x['track']) for x in l)
    lAlb = max(len(x['album']) for x in l)
    lMin = len('minutes')

    linie = '+ ' + '-' * lArt + ' + ' + '-' * lTrc + ' + '+ '-' * lAlb + ' + '+ '-' * lMin + ' +'
    print linie
    print '| ' + 'artist' + ' ' * (lArt - len('artist')) + ' | ' + 'track' + ' ' * (lTrc - len('track'))+' | '\
               + 'album' + ' ' * (lAlb - len('album'))+' | '+'minutes ' +'|'
    print linie

    for x in l:
        print '| ' + x['artist'] + ' ' * (lArt - len(x['artist'])) + ' | ' + x['track'] + ' ' * (lTrc - len(x['track'])) \
              + ' | ' + x['album'] + ' ' * (lAlb - len(x['album'])) + ' | ' \
              + str(x['minutes']) + ' ' * (lMin-1)  + ' | '
        print linie


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('list-artists', help='list the artists in a page ', type=str)
    parser.add_argument('-page','--page', help='Page to list', type=str)
    parser.add_argument('page', help='page number', type=int)
    args=parser.parse_args()

    if args.list-artists:
        a = [x['Name'] for x in getArtistsPage(args.page)]
    printArtists(a)

main()