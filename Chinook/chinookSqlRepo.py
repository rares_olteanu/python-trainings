import sqlite3

def create_connection():
    try:
        conn = sqlite3.connect('./db/chinook.db')
        print conn
        return conn
    except Exception  as e:
        print(e.message)
    return None

def getArtistsPage(pag):
    p=pag*12
    data = []
    try:
        conn = create_connection()
        if conn is not None:
            cursor = conn.cursor()
            cursor.execute("SELECT name FROM artists LIMIT {} OFFSET {};".format(12, p))
            rows = cursor.fetchall()
            for row in rows:
                data.append({
                             'Name': row[0]
                             })
    except conn.Error:
        conn.rollback()
        raise Exception ('Could not load artists')
    finally:
        conn.close()
        return data

def getArtistsTracks(t):
    data = []
    conn = create_connection()
    if conn is not None:
        cursor = conn.cursor()
        cursor.execute('''SELECT artists.name as artist,
                                    tracks.name as track,
                                    albums.title as album,
                                    tracks.milliseconds/1000 as seconds
                            from tracks
                            join albums on tracks.trackid=albums.albumid
                            join artists on albums.albumid=artists.artistid
                            WHERE seconds>{}
                         '''.format(t * 60))
        rows = cursor.fetchall()
        for row in rows:
            data.append({'artist': row[0],
                         'track': row[1],
                         'album': row[2],
                         'minutes': row[3]/60
                         })
    conn.close()
    return data



