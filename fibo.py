
def fib(n):
    if n <2:
        return 1
    else:
        return fib(n-1) + fib(n-2)

def fiboiterative(n):
    a,b=0,1
    l=[a,b]
    while a+b<=n:
        a,b=b,a+b
        l.append(b)
    return l
def scrie(n):
    a=[0]
    i=0
    while fib(i)<=n:
        a.append(fib(i))
        i+=1
    return a

import sys
param = int(sys.argv[1])
print scrie(param)
print fiboiterative(param)
assert scrie(10) == [0, 1, 1, 2, 3, 5, 8]
assert fiboiterative(10) == [0, 1, 1, 2, 3, 5, 8]
assert scrie(7) == [0, 1, 1, 2, 3, 5]
assert fiboiterative(7) == [0, 1, 1, 2, 3, 5]
