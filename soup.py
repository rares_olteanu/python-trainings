from bs4 import BeautifulSoup
import urllib2


def getUrls(url,word,number_of_articles):
    response = urllib2.urlopen(url,'html.parser')
    html = response.read()
    soup = BeautifulSoup(html,'html.parser')
    counter = 0
    l = []
    for link in soup.find_all("a"):
        if word in str(link) and 'title' in str(link):
            counter += 1
            if counter <= number_of_articles:
                l.append(link)
            else :
                break

    return l


def getContent(url,word,number_of_articles):
    url_list = getUrls(url,word,number_of_articles)
    l = []
    for url in url_list:
        html = urllib2.urlopen(url.attrs['href'], 'html.parser')
        soup = BeautifulSoup(html,'html.parser')
        for x in soup.find_all('div'):
            if x.get('id') == "articleContent":
                l.append(x.getText())
    return l


def printContent(content):
    for article in content :
        print article + "\n"

if __name__ == "__main__":
    content = getContent('http://hotnews.ro/','dna', 2)
    printContent(content)